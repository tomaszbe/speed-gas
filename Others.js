function stripPhones() {
  var range = SpreadsheetApp.getActiveSheet().getRange('I2:I')
  
  var values = range.getValues()
  
  for (var i in values) {
    
    if (values[i][0] === '') continue
    
    values[i][0] = values[i][0].split(' ').slice(0, 2).join(' ')
    
  }
  
  range.setValues(values)
  
}