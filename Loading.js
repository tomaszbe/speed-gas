function loadSheets(cars, folderId) {
  var files = DriveApp.getFolderById(folderId).getFiles()
  var reservationSheets = []
  var ztSheets = []
  while(files.hasNext()) {
    var file = files.next()
    var ss = SpreadsheetApp.open(file)
    ss.setSpreadsheetTimeZone('Europe/Berlin')
    var sheet = ss.getSheets()[0]
    if (sheet.getName().indexOf('tmp') === 0) {
      loadKeratronik(cars, sheet)
    } else if (sheet.getName().indexOf('Raport') === 0) {
      loadLincor(cars, sheet)
    } else if (sheet.getName().indexOf('Rez') === 0) {
      reservationSheets.push(sheet)
    } else if (sheet.getName().indexOf('Zlec') === 0) {
      ztSheets.push(sheet)
    }
  }
 
  for (var i in reservationSheets) {
    loadReservations(cars, reservationSheets[i])
  }
  
  for (var i in ztSheets) {
    loadZt(cars, ztSheets[i])
  }
  
}

function loadZt(cars, sheet) {
  var rows = sheet.getRange('A2:D').getValues()
  
  for (var rowNumber in rows) 
  {
    var row = rows[rowNumber]
    var plate = row[0]
    
    cars.insertZt(plate, ztRowToObject(row))
  }
}

function loadKeratronik(cars, sheet) {
  var rows = sheet.getDataRange().getValues()
  var read = false
  var type = null
  var fuelType = null
  
  for (var rowNumber in rows) {
    var row = rows[rowNumber]
    
    if (read && (row[0] !== '')) {
      read = false
    }
    
    if (!read) {
      if (row[0] === 'Prędkość > 130') {
        type = Alert.type.SPEED
        fuelType = FuelType.ALL
        read = true
        continue
      } else if (row[0] === 'Obroty > 4000') {
        type = Alert.type.REVS
        fuelType = FuelType.ON
        read = true
        continue
      } else if (row[0] === 'Obroty > 6000') {
        type = Alert.type.REVS
        fuelType = FuelType.PB
        read = true
        continue
      }
    }
    
    if (read) {
      var plate = row[1].split(' ')[0]
      if (!plate) {
        continue
      }
      
      cars.insertAlert(plate, Alert.keratronik(type, fuelType, row))
    }
  }
}

function loadLincor(cars, sheet) {
  var rows = sheet.getRange('A2:H').getValues()
  for (var rowNumber in rows) {
    var row = rows[rowNumber]
    
    if (row[0].indexOf('Ex') < 0) {
      continue
    }
    
    var plate = row[1]
    
    if (plate === '') {
      continue
    }
    
    cars.insertAlert(plate, Alert.lincor(row))
  }
  
}

function loadReservations(cars, sheet) {
  var rows = sheet.getRange('A2:E').getValues()
  for (var rowNumber in rows) 
  {
    var row = rows[rowNumber]
    var plate = row[0]
    
    cars.insertReservation(plate, reservationRowToObject(row))
  }
}

function ztRowToObject(row) {
  return { begin: row[1].getTime() === row[2].getTime() ? new Date(row[1] - 60 * 60000) : row[1], end: row[2], driver: row[3] }
}

function reservationRowToObject(row) {
  var o = { from: { date: Moment.moment(row[1], config.get('rmsDateFormat')), branch: row[2] } }
  if (row[3]) {
    o.to = { date: Moment.moment(row[3], config.get('rmsDateFormat')), branch: row[4] }
  }
  return o
}