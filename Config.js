var Config = function(data) {
  this.keys = {}
  
  for (i in data) {
    var row = data[i]
    if (row[0] === '') {
      continue
    }
    this.set(row[0], row[1])
  }
}

Config.prototype.set = function(key, value) {
  this.keys[key] = value
}

Config.prototype.get = function(key) {
  return this.keys[key]
}

Config.loadFromRange = function(range) {
  var data = range.getValues()
  return new Config(data)
}