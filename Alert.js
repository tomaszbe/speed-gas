var Alert = function(type, fuelType, date, address, value, gps) {
  this.type = type
  this.fuelType = fuelType
  this.date = date
  this.address = address
  this.value = value
  this.gps = gps
}

Alert.lincor = function(data) {
  var type = Alert.type.SPEED
  var fuelType = FuelType.ALL

  if (data[0].indexOf('4') > -1) {
    type = Alert.type.REVS
    fuelType = FuelType.ON
  } else if (data[0].indexOf('6') > -1) {
    type = Alert.type.REVS
    fuelType = FuelType.PB
  }

  return new Alert(
    type,
    fuelType,
    Moment.moment(data[5], config.get('lincorDateFormat')),
    data[3],
    data[7].split('/')[0],
    data[4].split(';')
  )
}

Alert.keratronik = function(type, fuelType, data) {
  var address = data[5] ? data[5] : data[7]
  address = address
    .split(', ')
    .reverse()
    .join(', ')
  return new Alert(type, fuelType, data[4], address)
}

Alert.prototype.isAutobahn = function() {
  var chars = this.address.substring(0, 2)
  return chars[0] === 'A' && (!isNaN(chars[1]) || chars[1] === 'u')
}

Alert.prototype.isSmallAutobahn = function() {
  return this.isAutobahn() && this.value !== undefined && this.value < 140
}

Alert.prototype.overlaps = function(reservation) {
  if (this.date.isAfter(reservation.from.date)) {
    if (!reservation.to || this.date.isBefore(reservation.to.date)) {
      return true
    }
  }
  return false
}

Alert.prototype.overlapsAny = function(reservations) {
  for (var i in reservations) {
    if (this.overlaps(reservations[i])) {
      return true
    }
  }
  return false
}
