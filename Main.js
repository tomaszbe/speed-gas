var config
function main() {
  var configRange = SpreadsheetApp.getActive().getSheetByName('Ustawienia').getDataRange()
  config = Config.loadFromRange(configRange)
  
  var rootId = config.get('root')
  var dieselsId = config.get('diesels')
  
  var diesels = SpreadsheetApp.openById(dieselsId).getSheets()[0].getDataRange().getValues()
  var diesels = diesels.reduce(function(all, current) {
    all.push(current[0])
    return all
  }, [])
  
  var cars = new Cars()
  
  loadSheets(cars, rootId)
  cars.markDiesels(diesels)
  
  var alerts = cars.getRelevantAlerts()
  var timestamp = Date.now()
  
  
  var toProcess = cars.toProcess
  if (toProcess.length > 0) {
    var toProcessSheet = SpreadsheetApp.getActive().insertSheet('Prędkości ' + timestamp, 0)
    toProcessSheet.deleteRows(2, toProcessSheet.getMaxRows() - 1)
    toProcessSheet.insertRowsAfter(1, toProcess.length)
    toProcessSheet.getRange('A1:D1').setValues([['id', 'speed', 'lat', 'lon']])
    toProcessSheet.getRange('A2:D').setValues(toProcess)
  }
  
  var results = SpreadsheetApp.getActive().insertSheet('Wynik ' + timestamp, 0)
  results.deleteColumns(7, results.getMaxColumns() - 6)
  results.deleteRows(2, results.getMaxRows() - 1)
  results.insertRowsAfter(1, alerts.length)
  results.getRange('A1:M1').setValues([['Samochód', 'Obroty', 'Prędkość', 'Limit', 'Przek.', 'Liczba', 'Początek', 'Koniec', 'Adres pierwszego', 'Adres ostatniego', 'Oddział', 'Kierowca', 'Uwagi']])
  results.getRange('A2:L').setValues(alerts)
  
  var foo = 'bar'
}



function findDriver(date, ztList) {
  for (var i in ztList) {
    var zt = ztList[i]
    if (date > zt.begin && date < zt.end) {
      zt.driver = zt.driver.replace('  ', ' ')
      return zt.driver.split(' ').slice(0, 2).join(' ')
    }
  }
  return null
}
