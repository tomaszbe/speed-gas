function loadLimits() {
  var data = SpreadsheetApp.getActive()
    .getSheetByName('out')
    .getDataRange()
    .getValues()

  data = data.slice(1).reduce(function(result, row) {
    result[row[0]] = result[row[0]] || []
    result[row[0]].push({
      speed: row[1],
      lat: row[2],
      lon: row[3],
      limit: row[4]
    })
    return result
  }, [])

  var resultRange = SpreadsheetApp.getActive()
    .getSheets()[0]
    .getRange('D2:E')

  var result = Array.apply(null, Array(resultRange.getLastRow() - 1))
  result = result.map(function() {
    return [null, null]
  })

  for (var key in data) {
    var maxRow = data[key].sort(limitRowCompare)[0]

    var mapUrl =
      'https://www.google.pl/maps/search/' + maxRow.lat + ',' + maxRow.lon
    var linkText = maxRow.speed + ' / ' + maxRow.limit
    var link = '=hyperlink("' + mapUrl + '"; "' + linkText + '")'

    result[parseInt(key)] = [
      //      maxRow.speed,
      link,
      (maxRow.speed - maxRow.limit) / maxRow.limit
    ]
  }

  resultRange.setValues(result)

  //  var foo = ''
}

function overLimitValue(row) {
  return row.speed / row.limit
}

function limitRowCompare(row1, row2) {
  return overLimitValue(row2) - overLimitValue(row1)
}
