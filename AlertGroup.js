var AlertGroup = function(plate, fuelType, alert) {
  this.plate = plate
  this.fuelType = fuelType
  this.type = alert.type
  this.firstDate = alert.date
  this.lastDate = alert.date
  this.firstAddress = alert.address
  this.lastAddress = alert.address
  this.count = 1
  this.maxRevs = alert.type === Alert.type.REVS ? alert.value : null
  this.maxSpeed = alert.type === Alert.type.SPEED ? alert.value : null
  this.branch = null
  this.driver = null
  this.toProcess = []
  this.addToProcess(alert)
}

AlertGroup.prototype.toArray = function() {
  return [
    this.plate,
//    this.formatType(),
    this.type & Alert.type.REVS ? (this.maxRevs ? this.maxRevs : (this.fuelType === FuelType.PB ? '>6000' : '>4000')) : null,
    this.type & Alert.type.SPEED ? (this.maxSpeed ? this.maxSpeed : '>130') : null,
    null, null, // Overspeed
    this.count,
    this.firstDate,
    this.lastDate,
    this.firstAddress.split(', ').slice(0, -1).join(', '),
    this.lastAddress.split(', ').slice(0, -1).join(', '),
    this.branch,
    this.driver
  ]
}

AlertGroup.prototype.formatType = function() {
  var strings = []
  var doString = this.count > 1 ? ' do ' : ': '
  if (this.type & Alert.type.REVS) {
    var string = 'Obroty'
    string += this.maxRevs ? (doString + this.maxRevs + (this.fuelType === FuelType.PB ? '/6000' : '/4000') ) : (this.fuelType === FuelType.PB ? ' > 6000' : ' > 4000')
    strings.push(string)
  }
  if (this.type & Alert.type.SPEED) {
   var string = 'Prędkość'
   string += this.maxSpeed ? (doString + this.maxSpeed + ' km/h') : ' > 130 km/h'
   strings.push(string)
  }
  return strings.join(', ')
}

AlertGroup.prototype.assignBranch = function(reservations) {
  this.branch = this.closestBranch(reservations)
}

AlertGroup.prototype.assignDriver = function(ztList) {
  this.driver = findDriver(this.meanDate(), ztList)
  if (!this.driver) {
    this.driver = findDriver(this.lastDate, ztList)
  }
}
    
AlertGroup.prototype.belongs = function(alert) {
  return alert.date - this.lastDate < 30 * 60000
}

AlertGroup.prototype.add = function(alert) {
  ++this.count
  this.lastDate = alert.date
  this.lastAddress = alert.address
  this.type |= alert.type
  if (alert.value) {
    if (alert.type === Alert.type.REVS && alert.value > this.maxRevs) {
      this.maxRevs = alert.value
    } else if (alert.type === Alert.type.SPEED && alert.value > this.maxSpeed) {
      this.maxSpeed = alert.value
    }
  }
  this.addToProcess(alert)
}

AlertGroup.prototype.addToProcess = function(alert) {
  if (alert.gps && alert.type === Alert.type.SPEED) {
    this.toProcess.push({
      speed: alert.value,
      gps: alert.gps
    })
  }
}

AlertGroup.prototype.addIfBelongs = function(alert) {
  if (this.belongs(alert)) {
    this.add(alert)
    return true
  }
  return false
}

AlertGroup.prototype.meanDate = function() {
  return new Date((this.firstDate.getTime() + this.lastDate.getTime()) / 2)
}

AlertGroup.group = function (plate, fuelType, alerts) {
  alerts.sort(function (alert1, alert2) {
      return alert1.date - alert2.date
    })
  
  return alerts.reduce( function(groups, alert) {
    if (groups.length === 0 || !groups[groups.length - 1].addIfBelongs(alert)) {
      groups.push(new AlertGroup(plate, fuelType, alert))
    }
    return groups
  }, [])
}

AlertGroup.prototype.closestBranch = function(reservations) {
  var date = this.meanDate()
  if (reservations.length === 0) {
    return '?'
  }
  var closest = Math.abs(date - reservations[0].from.date)
  var branch = reservations[0].from.branch
  
  for (var i in reservations) {
    if (Math.abs(date - reservations[i].from.date) < closest) {
      closest = Math.abs(date - reservations[i].from.date)
      branch = reservations[i].from.branch
    }
    if (reservations[i].to && Math.abs(date - reservations[i].to.date) < closest) {
      closest = Math.abs(date - reservations[i].to.date)
      branch = reservations[i].to.branch
      if (branch.indexOf('ASS') === 0) {
        branch = reservations[i].from.branch
      }
    }
  }
  return branch
}
