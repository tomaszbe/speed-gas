var Car = function(plate) {
  this.plate = plate
  this.reservations = []
  this.alerts = []
  this.ztList = []
  this.fuelType = FuelType.PB
}

Car.prototype.markAsDiesel = function() {
  this.fuelType = FuelType.ON
}

Car.prototype.isAlertRelevant = function(alert) {
  return (
    this.isAlertFuelTypeOk(alert) &&
    !alert.overlapsAny(this.reservations) &&
    !alert.isSmallAutobahn()
  )
}

Car.prototype.isAlertFuelTypeOk = function(alert) {
  return !!(alert.fuelType & this.fuelType)
}
