var Cars = function() {
  this.cars = []
  this.toProcess = []
}

Cars.prototype.getRelevantAlerts = function() {
  var result = []

  for (var plate in this.cars) {
    var car = this.cars[plate]

    var carAlerts = car.alerts.filter(function(alert) {
      return car.isAlertRelevant(alert)
    })

    var groups = AlertGroup.group(plate, car.fuelType, carAlerts)

    for (var i in groups) {
      groups[i].assignBranch(car.reservations)

      if (groups[i].branch.length < 2) {
        continue
      }

      groups[i].assignDriver(car.ztList)

      var toProcess = groups[i].toProcess

      for (var j in toProcess) {
        this.toProcess.push([
          result.length,
          toProcess[j].speed,
          toProcess[j].gps[0],
          toProcess[j].gps[1]
        ])
      }

      result.push(groups[i].toArray())
    }
  }

  return result
}

Cars.prototype.markDiesels = function(diesels) {
  for (var plate in this.cars) {
    if (binarySearch(diesels, plate) > -1) {
      this.cars[plate].markAsDiesel()
    }
  }
}

Cars.prototype.insertAlert = function(plate, alert) {
  this.get(plate, true).alerts.push(alert)
}

Cars.prototype.insertReservation = function(plate, reservation) {
  var car = this.get(plate)
  if (car !== undefined) {
    car.reservations.push(reservation)
  }
}

Cars.prototype.insertZt = function(plate, zt) {
  var car = this.get(plate)
  if (car !== undefined) {
    car.ztList.push(zt)
  }
}

Cars.prototype.get = function(plate, canCreate) {
  if (canCreate === undefined) {
    canCreate = false
  }

  var car = this.cars[plate]

  if (car === undefined && canCreate) {
    car = new Car(plate)
    this.cars[plate] = car
  }

  return car
}
